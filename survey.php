<?php
if (!empty($_SERVER['HTTP_X_REQUESTED_WITH'])
    && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'
    && $_POST) {

    header('Content-Type: application/json');
    $dataValid = true;

    foreach ($_POST as $key => $value) {
        if (strlen(trim($value)) == 0) {
            $dataValid = false;
        }
    }

    if ($dataValid) {
        $fileName = fopen("data.log", "w");
        fwrite($fileName, json_encode($_POST));
        fclose($fileName);
        echo json_encode([
            'status' => 'success',
            'message' => 'Thanks for your answers'
        ]);
    } else {
        echo json_encode([
            'status' => 'incomplete_data',
            'message' => 'Please fill in all the fields'
        ]);
    }
} else {
    ?>
    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Survey</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link href='http://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
        <link rel="icon" type="image/png" href="https://cdn.css-tricks.com/favicon-16x16.png" sizes="16x16">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous">

        <style>
            body {
                font-family: 'Roboto', sans-serif;
                margin: 0;
                padding: 0;
                font-size: 16px;
            }

            fieldset {
                border: none;
                text-align: center;
                max-width: 500px;
                margin: 5em auto 2em auto;
            }

            h2 {
                line-height: 2;
                text-align: center;
            }

            .submit-wrapper {
                text-align: left;
            }

            .submit-button {
                min-width: 150px;
                padding: .5em;
            }

            @media (max-width: 768px) {
                fieldset {
                    max-width: 100%;
                    margin: 5em 2em 2em 2em;
                }

                h2 {
                    font-size: 1em;
                    font-weight: bold;
                    line-height: 1.2;
                    margin-top: 2em;
                    margin-bottom: 2em;
                }

                label {
                    display: none;
                }

                .submit-wrapper {
                    text-align: center;
                }

                .submit-button {
                    margin-top: .5em;
                }
            }
        </style>
    </head>
    <body>
    <fieldset>
        <form class="form-horizontal" id="surveyForm">
            <div class="form-group">
                <label for="name" class="control-label col-sm-3">Your name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="name" name="name"
                           placeholder="Please leave your name here">
                </div>
            </div>
            <h2>What are your 3 major life goals?</h2>
            <div class="form-group">
                <label for="goal1" class="control-label col-sm-3">Goal #1:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="goal1" name="goal1" placeholder="Put your goal #1 here">
                </div>
            </div>
            <div class="form-group">
                <label for="goal2" class="control-label col-sm-3">Goal #2:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="goal2" name="goal2" placeholder="Put your goal #2 here">
                </div>
            </div>
            <div class="form-group">
                <label for="goal3" class="control-label col-sm-3">Goal #3:</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="goal3" name="goal3" placeholder="Put your goal #3 here">
                </div>
            </div>
            <div class="form-group">
                <div class="control-label col-sm-3"></div>
                <div class="col-sm-9 submit-wrapper">
                    <button type="submit" class="btn btn-primary submit-button">Submit</button>
                </div>
            </div>
        </form>
    </fieldset>

    <div id="responseModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" id="responseTitle">Success</h4>
                </div>
                <div class="modal-body">
                    <p id="responseMessage">Thanks for your answers</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-3.3.1.min.js"
            integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="js/vendor/jquery-3.3.1.min.js"><\/script>')</script>
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

    <script>
        $(function () {
            $("#surveyForm").submit(function (event) {
                event.preventDefault();
                var data = $(this).serializeArray();
                $.ajax({
                    url: 'survey.php',
                    method: 'POST',
                    data: data,
                    success: function (response) {
                        $('#responseMessage').html(response.message);

                        if (response.status === 'incomplete_data') {
                            $('#responseTitle').html('Incomplete data');
                        } else {
                            $('#responseTitle').html('Success');
                            $('#name').val('');
                            $('#goal1').val('');
                            $('#goal2').val('');
                            $('#goal3').val('');
                        }

                        $('#responseModal').modal('show');
                    }
                });
            });
        });
    </script>
    </body>
    </html>
<?php
}
?>